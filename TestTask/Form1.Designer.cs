﻿namespace TestTask
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPing1 = new System.Windows.Forms.Button();
            this.labelPingResult1 = new System.Windows.Forms.Label();
            this.btnAddIP = new System.Windows.Forms.Button();
            this.textBoxIP1 = new System.Windows.Forms.TextBox();
            this.FLPanelPingResult = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelRunningState = new System.Windows.Forms.Label();
            this.btnPingRange = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelInputIpRange = new System.Windows.Forms.Label();
            this.textBoxIPRangeStart = new System.Windows.Forms.TextBox();
            this.labelRunningStateRange = new System.Windows.Forms.Label();
            this.textBoxIPRangeEnd = new System.Windows.Forms.TextBox();
            this.FLPanelPingRangeResult = new System.Windows.Forms.FlowLayoutPanel();
            this.labelPingRangeResult = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.FLPanelPingResult.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.FLPanelPingRangeResult.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPing1
            // 
            this.btnPing1.Location = new System.Drawing.Point(153, 19);
            this.btnPing1.Name = "btnPing1";
            this.btnPing1.Size = new System.Drawing.Size(75, 23);
            this.btnPing1.TabIndex = 2;
            this.btnPing1.Text = "Ping all IP";
            this.btnPing1.UseVisualStyleBackColor = true;
            this.btnPing1.Click += new System.EventHandler(this.btnPing_Click);
            // 
            // labelPingResult1
            // 
            this.labelPingResult1.AutoSize = true;
            this.FLPanelPingResult.SetFlowBreak(this.labelPingResult1, true);
            this.labelPingResult1.Location = new System.Drawing.Point(3, 0);
            this.labelPingResult1.Name = "labelPingResult1";
            this.labelPingResult1.Size = this.btnPing1.Size;
            this.labelPingResult1.TabIndex = 3;
            this.labelPingResult1.Text = "Ping result";
            // 
            // btnAddIP
            // 
            this.btnAddIP.Location = new System.Drawing.Point(12, 19);
            this.btnAddIP.Name = "btnAddIP";
            this.btnAddIP.Size = new System.Drawing.Size(75, 23);
            this.btnAddIP.TabIndex = 0;
            this.btnAddIP.Text = "Add IP";
            this.btnAddIP.UseVisualStyleBackColor = true;
            this.btnAddIP.Click += new System.EventHandler(this.BtnAddIP_Click);
            // 
            // textBoxIP1
            // 
            this.textBoxIP1.Location = new System.Drawing.Point(3, 3);
            this.textBoxIP1.Name = "textBoxIP1";
            this.textBoxIP1.Size = new System.Drawing.Size(107, 20);
            this.textBoxIP1.TabIndex = 0;
            this.textBoxIP1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIP1_KeyPress);
            // 
            // FLPanelPingResult
            // 
            this.FLPanelPingResult.AutoScroll = true;
            this.FLPanelPingResult.Controls.Add(this.labelPingResult1);
            this.FLPanelPingResult.Location = new System.Drawing.Point(141, 3);
            this.FLPanelPingResult.Name = "FLPanelPingResult";
            this.FLPanelPingResult.Size = new System.Drawing.Size(294, 355);
            this.FLPanelPingResult.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.FLPanelPingResult);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(6, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 361);
            this.panel1.TabIndex = 5;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoScroll = true;
            this.flowLayoutPanel2.Controls.Add(this.textBoxIP1);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(132, 355);
            this.flowLayoutPanel2.TabIndex = 3;
            this.flowLayoutPanel2.WrapContents = false;
            // 
            // labelRunningState
            // 
            this.labelRunningState.AutoSize = true;
            this.labelRunningState.Location = new System.Drawing.Point(254, 24);
            this.labelRunningState.Name = "labelRunningState";
            this.labelRunningState.Size = new System.Drawing.Size(120, 13);
            this.labelRunningState.TabIndex = 6;
            this.labelRunningState.Text = "Ping is not executed yet";
            // 
            // btnPingRange
            // 
            this.btnPingRange.Location = new System.Drawing.Point(220, 38);
            this.btnPingRange.Name = "btnPingRange";
            this.btnPingRange.Size = new System.Drawing.Size(75, 23);
            this.btnPingRange.TabIndex = 8;
            this.btnPingRange.Text = "Ping range";
            this.btnPingRange.UseVisualStyleBackColor = true;
            this.btnPingRange.Click += new System.EventHandler(this.btnPingRange_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.labelInputIpRange);
            this.panel2.Controls.Add(this.textBoxIPRangeStart);
            this.panel2.Controls.Add(this.labelRunningStateRange);
            this.panel2.Controls.Add(this.textBoxIPRangeEnd);
            this.panel2.Controls.Add(this.FLPanelPingRangeResult);
            this.panel2.Controls.Add(this.btnPingRange);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(460, 413);
            this.panel2.TabIndex = 9;
            // 
            // labelInputIpRange
            // 
            this.labelInputIpRange.AutoSize = true;
            this.labelInputIpRange.Location = new System.Drawing.Point(9, 12);
            this.labelInputIpRange.Name = "labelInputIpRange";
            this.labelInputIpRange.Size = new System.Drawing.Size(141, 13);
            this.labelInputIpRange.TabIndex = 10;
            this.labelInputIpRange.Text = "Input IP Range in text boxes";
            // 
            // textBoxIPRangeStart
            // 
            this.textBoxIPRangeStart.Location = new System.Drawing.Point(10, 40);
            this.textBoxIPRangeStart.Name = "textBoxIPRangeStart";
            this.textBoxIPRangeStart.Size = new System.Drawing.Size(90, 20);
            this.textBoxIPRangeStart.TabIndex = 0;
            this.textBoxIPRangeStart.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIP1_KeyPress);
            this.textBoxIPRangeStart.Leave += new System.EventHandler(this.textBoxIPRangeStart_Leave);
            // 
            // labelRunningStateRange
            // 
            this.labelRunningStateRange.AutoSize = true;
            this.labelRunningStateRange.Location = new System.Drawing.Point(310, 43);
            this.labelRunningStateRange.Name = "labelRunningStateRange";
            this.labelRunningStateRange.Size = new System.Drawing.Size(120, 13);
            this.labelRunningStateRange.TabIndex = 9;
            this.labelRunningStateRange.Text = "Ping is not executed yet";
            // 
            // textBoxIPRangeEnd
            // 
            this.textBoxIPRangeEnd.AcceptsTab = true;
            this.textBoxIPRangeEnd.Location = new System.Drawing.Point(115, 40);
            this.textBoxIPRangeEnd.Name = "textBoxIPRangeEnd";
            this.textBoxIPRangeEnd.Size = new System.Drawing.Size(90, 20);
            this.textBoxIPRangeEnd.TabIndex = 1;
            this.textBoxIPRangeEnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIP1_KeyPress);
            this.textBoxIPRangeEnd.Enter += new System.EventHandler(this.textBoxIPRangeEnd_Enter);
            // 
            // FLPanelPingRangeResult
            // 
            this.FLPanelPingRangeResult.AutoScroll = true;
            this.FLPanelPingRangeResult.Controls.Add(this.labelPingRangeResult);
            this.FLPanelPingRangeResult.Location = new System.Drawing.Point(6, 79);
            this.FLPanelPingRangeResult.Name = "FLPanelPingRangeResult";
            this.FLPanelPingRangeResult.Size = new System.Drawing.Size(449, 329);
            this.FLPanelPingRangeResult.TabIndex = 1;
            // 
            // labelPingRangeResult
            // 
            this.labelPingRangeResult.AutoSize = true;
            this.FLPanelPingRangeResult.SetFlowBreak(this.labelPingRangeResult, true);
            this.labelPingRangeResult.Location = new System.Drawing.Point(3, 0);
            this.labelPingRangeResult.Name = "labelPingRangeResult";
            this.labelPingRangeResult.Size = this.btnPing1.Size;
            this.labelPingRangeResult.TabIndex = 3;
            this.labelPingRangeResult.Text = "Ping result";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(474, 445);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.btnAddIP);
            this.tabPage1.Controls.Add(this.btnPing1);
            this.tabPage1.Controls.Add(this.labelRunningState);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(466, 419);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ping separate IP";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(466, 419);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ping range of IP";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 445);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Ping and HTTP request program";
            this.FLPanelPingResult.ResumeLayout(false);
            this.FLPanelPingResult.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.FLPanelPingRangeResult.ResumeLayout(false);
            this.FLPanelPingRangeResult.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

       

        #endregion

        private System.Windows.Forms.Button btnAddIP;
        private System.Windows.Forms.Button btnPing1;
        private System.Windows.Forms.TextBox textBoxIP1;
        private System.Windows.Forms.Label labelPingResult1;
        private System.Windows.Forms.FlowLayoutPanel FLPanelPingResult;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label labelRunningState;
        private System.Windows.Forms.Button btnPingRange;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel FLPanelPingRangeResult;
        private System.Windows.Forms.Label labelPingRangeResult;
        private System.Windows.Forms.TextBox textBoxIPRangeStart;
        private System.Windows.Forms.TextBox textBoxIPRangeEnd;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label labelRunningStateRange;
        private System.Windows.Forms.Label labelInputIpRange;
    }
}

