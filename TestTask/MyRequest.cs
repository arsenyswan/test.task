﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using System.Threading;
namespace TestTask
{
    class MyRequest
    {
        IPAddress ipAddress = null;
        public bool pingSuccess = false;
        string requestUri;
        WebRequest request;
        public string responseFromServer{get;private set;}
        public bool goodIpFormat;
        HttpWebResponse response;
        public void pingAndRequest()
        {
            responseFromServer = ipAddress.ToString();
            try
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();
                for (int i = 0; i < 4; i++)//4 пинга с задержкой
                {
                    options.DontFragment = true;
                    // Создайем буфер объемом 32 байта для передачи данных.
                    string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    byte[] buffer = Encoding.ASCII.GetBytes(data);
                    int timeout = 120;
                    PingReply reply = pingSender.Send(ipAddress, timeout, buffer, options);
                    //Если пинг удался
                    if (reply.Status == IPStatus.Success)
                    {
                        responseFromServer += " Ping was successful.";
                        pingSuccess = true;
                        requestHTTP();
                        break;
                    }
                    Thread.Sleep(50);
                }
                //если пинг не удался
                if (pingSuccess == false)
                {
                    responseFromServer += " Ping failed";
                }
            }
            catch (Exception ex)
            {
                responseFromServer += " " + ex.Message;
            }
        }
        

        private void requestHTTP()
        {
            requestUri = "http://" + ipAddress + "/cgi-bin/admin/privacy.cgi";
            request = WebRequest.Create(requestUri);
            request.Credentials = CredentialCache.DefaultCredentials;

            // Получаем ответ
            response = (HttpWebResponse)request.GetResponse();
            // Получение строки ответа
            //Stream dataStream = response.GetResponseStream();
            //// Открытие потока, используя StreamReader для легкого доступа
            //StreamReader reader = new StreamReader(dataStream);
            //responseFromServer += " " + reader.ReadToEnd();
            responseFromServer += " Проверка выполнена успешно";
            // закрываем поток и ответ.
            //reader.Close();
            //dataStream.Close();
            response.Close();
        }
        public MyRequest(string ipFromTextBox)
        {
            if (IPAddress.TryParse(ipFromTextBox, out ipAddress) && ipAddress.ToString().Length == ipFromTextBox.Length)
            {
                responseFromServer = ipFromTextBox + " - parsed ok\n";
                goodIpFormat = true;
                
            }
            else
            {
                responseFromServer = ipFromTextBox + ((ipFromTextBox.Length != 0) ? " - wrong ip Format" : "Ip field is empty\n");
                goodIpFormat = false;
            }
        }
    }
}
