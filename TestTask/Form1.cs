﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;

namespace TestTask
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        // Bндекс добавляемый к имени вновь созданного объекта Label и TextBox
        int indexOfNewObect = 2;
        
        delegate void SetTextCallback(Label label,string text);
        // Jбработчик кнопки BtnAddIP. Реализует добавление новых полей для ввода IP адресов и полей для вывода результата пинга
        private void BtnAddIP_Click(object sender, EventArgs e) 
        {
            TextBox textBoxIP = new TextBox();
            textBoxIP.Name = "textBoxIP" + indexOfNewObect.ToString();
            textBoxIP.Size = textBoxIP1.Size;
            textBoxIP.KeyPress += textBoxIP1_KeyPress;
            flowLayoutPanel2.Controls.Add(textBoxIP);
            Label labelPingResult = new Label();
            labelPingResult.Name = "labelPingResult" + indexOfNewObect.ToString();
            FLPanelPingResult.Controls.Add(labelPingResult);
            labelPingResult.Text = "";
            labelPingResult.AutoSize = true;
            FLPanelPingResult.SetFlowBreak(labelPingResult, true);
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            Task mainTaskForRequest = new Task(mainTask);
            mainTaskForRequest.Start();   
        }

        private void mainTask()
        {
            //List<Task<string>> tasksList = new List<Task<string>>();
            List<Task> tasksList = new List<Task>();
            List<MyRequest> myReqList= new List<MyRequest>();
            
            //очистка результатов предыдущего пинга
            foreach (Label label in FLPanelPingResult.Controls.OfType<Label>())
            {
                SetText(label, "");
            }
            List<string> strResultList = new List<string>();
            SetText(labelRunningState, "Trying to perform Ping and HTTP request");
            //пинг по всем ip адресам из всех текст боксов
            foreach (TextBox textBoxIP in flowLayoutPanel2.Controls.OfType<TextBox>())
            {
                //создаем объект которые будет выполнять запрос
                myReqList.Add(new MyRequest(textBoxIP.Text));
                //pingAndRequest();
                if (myReqList.Last().goodIpFormat)
                {
                    //добавляем его в список задач для параллельного выполнения и выполняем запрос
                    tasksList.Add(Task.Factory.StartNew(myReqList[myReqList.Count - 1].pingAndRequest));
                }
            }
        
            Task.WaitAll(tasksList.ToArray());

            SetText(labelRunningState, (myReqList.Any(req => req.pingSuccess == true) ? "Ping and HTTP Executed" : "No address available"));
            printResultToLabelList(myReqList, FLPanelPingResult);
        }

        private void printResultToLabelList(List<MyRequest> result, FlowLayoutPanel panelPingRes)
        {
            int i=0;
            foreach (Label label in panelPingRes.Controls.OfType<Label>())
            {
                if (i == result.Count)
                {
                    throw new Exception("flowLayoutPanel has odd Label elements");
                }
                this.SetText(label, result[i].responseFromServer);
                label.Text = result[i].responseFromServer;
                i++;
            }
        }
        //функция реализовывающий обратный вызов для возможности параллельно писать в Label не из того потока, в котором он создан
        private void SetText(Label label,string text)
        {
            if (label.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { label, text });
            }
            else
            {
                label.Text = text;
            }
        }
     
        private void textBoxIP1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(8))
            {
                string strIP = (sender as TextBox).Text;
                // запрещаем ввод не цифр и не точек
                if (!Char.IsDigit(e.KeyChar) && e.KeyChar != '.') 
                {
                    e.Handled = true;
                }
                // запрещаем ввод двух точек подряд
                if ((e.KeyChar == '.') && (strIP.Length < 1 || (sender as TextBox).Text[(sender as TextBox).Text.Length - 1] == '.'))
                {
                    e.Handled = true;
                }
                // в внутри этго блока могут быть уже только цифры
                if (e.KeyChar != '.') 
                {
                    // запрещаем ввод больше трех цифр подряд
                    if (strIP.LastIndexOf('.') + 3 < strIP.Length && strIP.Length > 1)
                    {
                        e.Handled = true;
                    }
                    // запрещаем ввод октетов больше 299
                    if ((strIP.Length == 2 && (int)Char.GetNumericValue(strIP[0]) > 2) || (strIP.Length > 1 && strIP[strIP.Length - 1] == '.' && ((int)Char.GetNumericValue(e.KeyChar) > 2)))
                    {
                        e.Handled = true;
                    }
                }
                //ограничиваем по длине
                if (strIP.Length>=15)
                {
                    e.Handled = true;
                }
            }
        }

        private void btnPingRange_Click(object sender, EventArgs e)
        {
            List<Task> tasksList = new List<Task>();
            List<MyRequest> myReqList = new List<MyRequest>();

            //очистка результатов предыдущего пинга
            SetText(labelPingRangeResult, "");
            List<string> strResultList = new List<string>();

            //пинг по всем ip адресам из диапазона
            //проверка на корректность 
            MyRequest m = new MyRequest(textBoxIPRangeStart.Text);
            MyRequest m2 = new MyRequest(textBoxIPRangeEnd.Text);
            int t = String.Compare(textBoxIPRangeStart.Text, textBoxIPRangeEnd.Text);
            if (m.goodIpFormat && m2.goodIpFormat && (String.Compare(textBoxIPRangeStart.Text, textBoxIPRangeEnd.Text) < 0 || textBoxIPRangeEnd.Text.Length > textBoxIPRangeStart.Text.Length))
            {
                int ipRangeStart = getLastByteIpAddres(textBoxIPRangeStart.Text);
                int ipRangeEnd = getLastByteIpAddres(textBoxIPRangeEnd.Text);
                SetText(labelRunningStateRange, "Trying to perform Ping and HTTP request");
                for (int i = ipRangeStart; i <= ipRangeEnd; i++)
                {
                    //создаем объект которые будет выполнять запрос
                    myReqList.Add(new MyRequest(textBoxIPRangeStart.Text.Substring(0, textBoxIPRangeStart.Text.LastIndexOf(".") + 1) + i.ToString()));
                    if (myReqList.Last().goodIpFormat)
                    {
                        //добавляем его в список задач для параллельного выполнения и выполняем запрос
                        tasksList.Add(Task.Factory.StartNew(myReqList[myReqList.Count - 1].pingAndRequest));
                    }
                }
            }
            else
            {
                labelPingRangeResult.Text += m.responseFromServer+ m2.responseFromServer;
                if ((String.Compare(textBoxIPRangeStart.Text, textBoxIPRangeEnd.Text) >= 0))
                {
                    labelPingRangeResult.Text += "Ip in left text box should be greather than ip in rigth text box";
                }
            }

            Task.WaitAll(tasksList.ToArray());

            SetText(labelRunningStateRange, (myReqList.Any(req => req.pingSuccess == true) ? "Ping and HTTP Executed" : "No address available"));
            foreach (var req in myReqList)
            {
                labelPingRangeResult.Text += req.responseFromServer + "\n";
            }
        }
        // вспомогательная функция для получения последнего октета IP адреса при работе с диапазоном адресов
        private int getLastByteIpAddres(string ipAddress)
        {
            int ipStrIndex = ipAddress.LastIndexOf(".") + 1;
            return  int.Parse(ipAddress.Substring(ipStrIndex,ipAddress.Length-ipStrIndex));
        }

        private void textBoxIPRangeStart_Leave(object sender, System.EventArgs e)
        {
            textBoxIPRangeEnd.Text = textBoxIPRangeStart.Text.Substring(0, textBoxIPRangeStart.Text.LastIndexOf(".") + 1);
        }
        private void textBoxIPRangeEnd_Enter(object sender, EventArgs e)
        {
            (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
        }
    }
}
